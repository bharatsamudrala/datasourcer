"""PUBMED: Process to download, extract and upload to S3"""

from ftplib import (
    error_perm
)
import os
import datetime
from utilities import (
    get_logger,
    get_s3_client,
    get_temp_working_directory,
    get_ftp_client,
    s3_exists,
    dbConnet
)
import pycurl


class PubmedScraper:

    def __init__(self):
        self.logger = get_logger(__name__)
        self.working_dir = get_temp_working_directory()
        self.s3_root = os.path.join(
            'data',
            datetime.datetime.now().strftime('%Y/%m/%d'),
            'structured',
            'raw'
            ) 
        self.bucket = 'mmcapturepoctemp'
        self.local_file = None
        self.cur = None

    def curl_http_dowlonad(self, login_url, username, password):
        drugbank_urls = ['https://portal.drugbankplus.com/downloads/drugbank_ontologies.zip/latest',
                         'https://portal.drugbankplus.com/downloads/DrugBank Drugs/latest']
        for url in drugbank_urls:
            try:
                c = pycurl.Curl()
                c.setopt(c.URL, login_url)
                c.setopt(pycurl.USERPWD, "%s:%s" % (username, password))
                c.setopt(pycurl.COOKIEFILE, 'cookie.txt')
                c.setopt(pycurl.USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36')
                c.setopt(c.FOLLOWLOCATION, True)
                c.perform()
                self.logger.info('Status: %d' % c.getinfo(c.RESPONSE_CODE))
                c.setopt(c.URL, url)
                local_dir = get_temp_working_directory()
                dbfile = url.split('/')[-2] + '.zip'
                file = open(os.path.join(local_dir, dbfile), 'wb')
                file.seek(0)
                c.setopt(c.WRITEDATA, file)
                c.perform()
                c.close()
            except pycurl.error as e:
                self.logger.info(url + 'download failed')
                self.logger.info('with error' + e)

    def ftp_download(self, path, source_url):
        ftp_client = get_ftp_client(source_url)
        ftp_client.login()

        try:
            ftp_client.cwd(path)
        except error_perm as e:
            self.logger.info('Error: %s' % e)
            return

        filelist = ftp_client.nlst()
        local_path = os.path.join(self.working_dir, path)
        s3client = get_s3_client()


        if not os.path.exists(local_path):
            os.makedirs(local_path)

        for pfile in filelist:
            s3_file_key = os.path.join(self.s3_root, pfile)
            if pfile.endswith('.g') and not s3_exists(s3client, self.bucket, s3_file_key):
                upload_to_s3 = True
                try:
                    self.local_file = os.path.join(local_path, pfile)
                    ftp_client.retrbinary("RETR " + pfile, open(
                        os.path.join(local_path, pfile), "wb").write)
                except error_perm:
                    self.logger.info(pfile + " download failed")
                else:
                    self.logger.info(pfile + " downloaded")
                    s3_file_root = os.path.join(self.s3_root, path, pfile)
                    if upload_to_s3:
                        s3client.upload_file(self.local_file, self.bucket, s3_file_root)

                    file_size = os.stat(os.path.join(local_path, pfile)).st_size
                    file_id = pfile.split('.')[0]
                    file_name = pfile

                    os.remove(self.local_file)
            elif pfile.endswith('.md5'):
                print("select file_name from pubmed_delta where file_name = '%s'" % pfile)
                if not bool(self.cur.execute("select file_name from pubmed_delta where file_name = '%s'" % pfile)):
                    ftp_client.retrbinary("RETR " + pfile, open(
                        os.path.join(local_path, pfile), "wb").write)
                    with open(os.path.join(local_path, pfile), 'r') as tempfile:
                        file_hash = tempfile.read()


        return

    def get_source_url(self, seedurlid):
        conn = dbConnet()
        self.cur = conn.cursor()
        self.cur.execute("select domainname from seedurl where seedurlid = '%s';" % seedurlid)
        domainname = self.cur.fetchall()
        for name in domainname:
            domain = name[0]
            return domain


def main():

    # source_list = [os.path.join('updatefiles'), os.path.join('baseline')]
    pub = PubmedScraper()
    # source_url = pub.get_source_url(seedurlid='307')
    # for source in source_list:
    #     pub.ftp_download(source, source_url)

    login_url = 'https://portal.drugbankplus.com/portal_users/sign_in'#pub.get_source_url(seedurlid='308')
    username = 'medmeme'
    password = '3tV_q8MT6rTD7YtrBe5y'
    pub.curl_http_dowlonad(login_url, username, password)

if __name__ == '__main__':
    main()

