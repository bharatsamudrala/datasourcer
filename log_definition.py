import logging
import boto3


logging.basicConfig(filename='master.log', level=logging.DEBUG, format='%(asctime)s \t[%(name)-s] \t%(filename)-s \t%(lineno)-d \t%(levelname)-s: \t%(message)s \r', datefmt='%Y-%m-%dT%T%Z')